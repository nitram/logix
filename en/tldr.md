---
title: tl;dr
---
## tl;dr

- [assembly video](/vid)
- [Assembly guide](/en/assembleyourcard10) - Assemble your card10
- [app](/app) - Companion app for Android and iOS
- [personal state](/ps)
- [Interhacktions](/en/interhacktions) - a guide to making apps
- [Hatchery](https://badge.team/badge/card10) - the card10 app store, publish your app
- [Hardware](http://git.card10.badge.events.ccc.de/card10/hardware) - hardware repo with docs and specs
- [Hardware Overview](/en/hardware-overview)  
- [USB-C](/en/usbc)
- [Firmware](http://git.card10.badge.events.ccc.de/card10/firmware) - firmware repo
- [Firmware Docs](https://firmware.card10.badge.events.ccc.de/) - rendered version of the firmware docs
- [LogBook](/en/logbook) - records of travellers

