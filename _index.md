---
title: Home
---
## Camp was wonderful, see you at 36C3
if you still have a voucher (white or blue), bring it to congress and you might be able to redeem it against one of the card10s we are reparing in the meantime.

No sale of card10s!

## current firmware version: Karotte (as of 2019-09-24)
It is very easy to [update your card10 firmware](/en/firmwareupdate) with just a USB-C cable. The update instructions
also contain a list of the existing firmware releases.

# card10

Welcome to card10 Wiki. You can [browse](https://git.card10.badge.events.ccc.de/card10/logix) this wiki source and [read more](/en/gitlab) about our GitLab instance.

**card10** is the name of the badge for the 2019 [Chaos Communication Camp](https://events.ccc.de/camp/2019/wiki/Main_Page). 

### [Getting started](/en/gettingstarted) 
You just received your card10, what now? The [Getting started](/en/gettingstarted) page has some advice how to assemble and use the card10.

### [First interhacktions](/en/firstinterhacktions)
After playing around with card10 a bit, you want to learn how to write your own software. Even if you have never programmed before, it is super easy to make some LEDs blink on card10 :)

### [tl;dr](/en/tldr)
Spare me the details just give me the links to the docs!

## Overview

> <img class="center" alt="Photo of the card10 badge" src="/media/ECKyISOXsAYDJZl.jpg"  width="420" height="auto" align="center">
>
> Hello, my name is card10

### Community
  - [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat): [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)
  - or [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)) (mirror): [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr)
  - [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

### Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)

### [FAQ](/en/faq)

### Events
  - Ongoing:
  - Upcoming:
    - card10 [workout in Berlin](en/workouts/berlin-2019-10-03): xHain, Thurs-Sun, October 3rd to 6th, 2019

  Events [archive](en/events).
